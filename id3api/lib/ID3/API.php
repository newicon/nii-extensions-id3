<?php

namespace ID3;

use \SoapClient;

class API
{
  
  static protected $wsdl = 'https://www.id3global.com/ID3gWS/ID3global.svc?wsdl';
  protected $client;                                       // handle to SOAP Client
  protected $wsse_header;
  protected $username;                                     // Account Username
  protected $password;                                     // Account Password
  protected $profile_id;                                   // Profile ID
  protected $exception;
  
  /**
   * Constructor
   * Create our soap client
   * @return null
   */
  function __construct($username, $password, $profile_id = null)
  {
    $wsse_header = new WsseAuthHeader($username,$password);
    $options = array(
      'soap_version'    => SOAP_1_1,
      'exceptions'      => true,
      'trace'           => 1,
      'cache_wsdl'      => WSDL_CACHE_NONE,
      'wdsl_local_copy' => false);

    try{
      $this->client     = new SoapClient(self::$wsdl, $options);
      $this->client->__setSoapHeaders(array($wsse_header));
      $this->username   = $username;
      $this->password   = $password;
      $this->profile_id = $profile_id;
    }catch(\SoapFault $e){
      $this->exception = $e;
    }
  }
  
  /**
   * getProfile
   */
  public function getProfile($version = null)
  {
    $data = array( 'AccountName'    => $this->username,
                   'Password'       => $this->password,
                   'ProfileID'      => $this->profile_id,
                   'ProfileVersion' => 0,
                 );
    $result = $this->soap_call('GetProfile',$data);
    return $result;
  }

  /**
   * getProfiles
   */
  public function getProfileVersions($version=null)
  {
    $data = array('AccountName'    => $this->username,
                  'Password'       => $this->password);

    $result = $this->soap_call('GetProfileVersions',$data);
    return $result;
  }

  /**
   * getPassportCountries
   */
  public function getPassportCountries()
  {
    $this->clearSoapHeaders();
    $result = $this->soap_call('GetPassportCountries');
    if(is_soap_fault($result))
      return $result;
    else
      return $this->objectToArray($result);
  }
  
  /**
   * checkCredentials
   */
  public function checkCredentials()
  {
    $data = array( 'AccountName' => $this->username,
                   'Password'    => $this->password );
    $result = $this->soap_call('CheckCredentials',$data);
    return $result;
  }
  
  /**
   * authenticateSP
   */
  public function authenticateSP(\ID3\DataLib\GlobalProfileIDVersion $profile, \ID3\DataLib\GlobalInputData $input_data, $customer_ref = null)
  {
    $data = array( 'ProfileIDVersion'   => $profile->asArray(),
                   'InputData'          => $input_data->asArray(),
                   'CustomerReference'  => $customer_ref);
    $result = $this->soap_call('AuthenticateSP',$data);
    return $result;
  }

  public function addressLookup(\ID3\DataLib\GlobalAddress $input_data)
  {
    $data = array('InputData' => $input_data->asArray());
    $result = $this->soap_call('AddressLookup',$data);
    if(is_soap_fault($result))
      return $result;
    else
      return $this->objectToArray($result);
  }
  
  /**
   * Helper function to call a method on the SoapClient and catch
   * any exceptions
   */
  private function soap_call($method_name,$data = array(),$authenticated = true)
  {
    // If we've already thrown an error, for example when connecting, then
    // just throw that straight back
    if(isset($this->exception))
      return $this->exception;

    try{
      $result = $this->client->$method_name( $data );
    }catch(\SoapFault $e){
      return $e;
    }
    return $result;
  }

  /**
   * Helper to clear SOAP headers
   * Can throw SoapFault if one has alreay been generated
  */
  private function clearSoapHeaders()
  {
    if(isset($this->exception))
      return $this->exception;
    $this->client->__setSoapHeaders(null);
  }

  /**
   * Helper function to convert a stdClass object to an array
   */
  private function objectToArray($d)
  {
    if(is_object($d))
      $d = get_object_vars($d);

    if(is_array($d))
      return array_map(array($this,'objectToArray'),$d);
    else
      return $d;
  }
}  
