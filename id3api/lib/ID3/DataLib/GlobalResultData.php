<?php

namespace ID3\DataLib;

class GlobalResultData extends GlobalBase
{
  public $properties = array('AuthenticationID'    => null,
                             'BandText'            => null,
                             'ChainID'             => null,
                             'ChainIDSpecified'    => null,
                             'CustomerRef'         => null,
                             'iProfileRevision'    => null,
                             'iProfileVersion'     => null,
                             'NoRetry'             => null,
                             'ProfileID'           => null,
                             'ProfileName'         => null,
                             'ProfileRevision'     => null,
                             'ProfileState'        => null,
                             'ProfileVersion'      => null,
                             'ResultCodes'         => null,
                             'Score'               => null,
                             'ScoreSpecified'      => null,
                             'Timestamp'           => null,
                             'UserBreakpoint'      => null,
                           );
}