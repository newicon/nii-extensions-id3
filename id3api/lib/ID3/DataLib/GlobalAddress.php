<?php

namespace ID3\DataLib;

class GlobalAddress extends GlobalBase
{
  public $properties = array(
    'Country'         => null,
    'Street'          => null,
    'SubStreet'       => null,
    'City'            => null,
    'SubCity'         => null,
    'StateDistrict'   => null,
    'POBox'           => null,
    'Region'          => null,
    'Principality'    => null,
    'ZipPostcode'     => null,
    'DpsZipPlus'      => null,
    'CedexMailsort'   => null,
    'Department'      => null,
    'Company'         => null,
    'Building'        => null,
    'SubBuilding'     => null,
    'Premise'         => null,
    'AddressLine1'    => null,
    'AddressLine2'    => null,
    'AddressLine3'    => null,
    'AddressLine4'    => null,
    'AddressLine5'    => null,
    'AddressLine6'    => null,
    'AddressLine7'    => null,
    'AddressLine8'    => null,
    'FirstYearOfResidence' => null,
    'LastYearOfResidence'  => null
    );
}