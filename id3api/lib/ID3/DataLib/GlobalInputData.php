<?php
  
namespace ID3\DataLib;

class GlobalInputData extends GlobalBase
{
  public $properties = array(
    'AddressDocuments'  => 'ID3\DataLib\GlobalAddressDocuments',
    'Addresses'         => 'ID3\DataLib\GlobalAddresses',
    'BankingDetails'    => 'ID3\DataLib\GlobalBankingDetails',
    'ContactDetails'    => 'ID3\DataLib\GlobalContactDetails',
    'Employment'        => 'ID3\DataLib\GlobalEmployment',
    'GlobalGeneric'     => 'ID3\DataLib\GlobalGeneric',
    'IdentityDocuments' => 'ID3\DataLib\GlobalIdentityDocuments',
    'Images'            => 'ID3\DataLib\GlobalImages',
    'Location'          => 'ID3\DataLib\GlobalLocation',
    'Personal'          => 'ID3\DataLib\GlobalPersonal',
  );
}