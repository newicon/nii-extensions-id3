<?php

namespace ID3\DataLib;

class GlobalLocation extends GlobalBase
{
  public $properties = array('AcceptHeaders'    => null,
                             'BlackBoxID'       => null,
                             'Country'           => null,
                             'IndividualID'     => null,
                             'IPAddress'        => null,
                             'State'            => null,
                             'UserAgent'        => null
                           );
  
}