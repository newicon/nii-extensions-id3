<?php

namespace ID3\DataLib;

class GlobalUKPassport extends GlobalBase
{
  public $properties = array(
    'Number'                  => null,
    'ExpiryDay'               => null,
    'ExpiryMonth'             => null,
    'ExpiryYear'              => null
    );

}