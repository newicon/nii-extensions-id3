<?php

namespace ID3\DataLib;

class GlobalContactDetails extends GlobalBase
{
  public $properties = array(
    'Email'            => null,
    'LandTelephone'    => 'ID3\DataLib\GlobalLandTelephone',
    'MobileTelephone'  => 'ID3\DataLib\GlobalMobileTelephone',
    'WorkTelephone'    => 'ID3\DataLib\GlobalWorkTelephone',
    );
}