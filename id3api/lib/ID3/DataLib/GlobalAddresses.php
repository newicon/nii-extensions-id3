<?php

namespace ID3\DataLib;

class GlobalAddresses extends GlobalBase
{
  public $properties = array('CurrentAddress'    => 'ID3\DataLib\GlobalAddress',
                             'PreviousAddress1'  => 'ID3\DataLib\GlobalAddress',
                             'PreviousAddress2'  => 'ID3\DataLib\GlobalAddress',
                             'PreviousAddress3'  => 'ID3\DataLib\GlobalAddress',
                             );
}