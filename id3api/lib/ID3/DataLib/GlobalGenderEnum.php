<?php

namespace ID3\DataLib;

class GlobalGenderEnum extends GlobalEnum
{
  const Unspecified = 'Unspecified';
  const Unknown     = 'Unknown';
  const Male        = 'Male';
  const Female      = 'Female';
}