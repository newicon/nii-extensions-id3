<?php

namespace ID3\DataLib;

class GlobalImages extends GlobalBase
{
  public $properties = array('DocImageId'    => null,
                             'Reference'     => null,
                             'Type'          => null);
}