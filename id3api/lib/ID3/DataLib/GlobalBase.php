<?php
namespace ID3\DataLib;

class GlobalBase
{
  private $data = array();
  
  public function __get($name)
  {
    // If we have no properties, return null
    if(empty($this->properties))
      return null;
    
    
    if(array_key_exists($name,$this->properties))
    {
      if((!isset($this->data[$name]) ||
         (is_null($this->data[$name]))))
      {
        if(!is_null($this->properties[$name]))
          $this->data[$name] = new $this->properties[$name];
      }
      return $this->data[$name];
    }
    
    return null;
  }
  
  public function __set($name,$value)
  {
    if(!array_key_exists($name,$this->properties))
      return null;
    
    if(is_null($value))
      return $this->data[$name] = null;
    
    $propertyClass = $this->properties[$name];
    
    // If the class is null, just set it
    if(is_null($propertyClass))
      return $this->data[$name] = $value;
     
    // If the class is set, make sure it's valid
    if($value instanceof $propertyClass)
      $this->data[$name] = $value;
    else if(is_subclass_of($propertyClass,'ID3\DataLib\GlobalEnum'))
    {
      // Check the enumerated value is valid
      if(defined("$propertyClass::$value"))
        $this->data[$name] = $value;
      else
        throw new \Exception("Enumeration mismatch $propertyClass and $value");
    }
    else
      throw new \Exception("Type mismatch " . get_class($value) . " vs $propertyClass");
  }
  
  public function asArray()
  {
    $a = array();
    foreach($this->data as $property => $type)
    {
      
      if(is_object($type) && 
         is_subclass_of($type, 'ID3\DataLib\GlobalBase'))
      { 
        //print_r($this->data[$property]);exit;
        $a[$property] = $this->data[$property]->asArray();
      }
      else
      {
        $a[$property] = $type;
      }
    }
    return $a;
  }
  
}