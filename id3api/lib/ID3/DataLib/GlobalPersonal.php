<?php

namespace ID3\DataLib;

class GlobalPersonal extends GlobalBase
{
  public $properties = array('AlternateName'    => 'ID3\DataLib\GlobalAlternateName',
                             'PersonalDetails'  => 'ID3\DataLib\GlobalPersonalDetails');
  
}