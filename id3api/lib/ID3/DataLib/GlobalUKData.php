<?php

namespace ID3\DataLib;

class GlobalUKData extends GlobalBase
{
  public $properties = array(
    'Passport'                  => 'ID3\DataLib\GlobalUKPassport',
    'DrivingLicence'            => 'ID3\DataLib\GlobalUKDrivingLicence',
    'NationalInsuranceNumber'   => 'ID3\DataLib\GlobalUKNationalInsuranceNumber',
    );  
}