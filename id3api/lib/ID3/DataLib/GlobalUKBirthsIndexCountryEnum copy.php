<?php

namespace ID3\DataLib;

class GlobalUKBirthsIndexCountryEnum extends GlobalEnum
{
  public $values = array('UNSPECIFIED','ENGLANDWALES','OTHER');
}