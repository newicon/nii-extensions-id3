<?php

namespace ID3\DataLib;

class GlobalProfileIDVersion extends GlobalBase
{
  public $properties = array('ID'       => null,
                             'Version'  => null);
  
}