<?php
  
namespace ID3\DataLib;

class GlobalIdentityDocuments extends GlobalBase
{
  public $properties = array(
    'InternationalPassport' => 'ID3\DataLib\GlobalInternationalPassport',
    'EuropeanIdentityCard'  => 'ID3\DataLib\GlobalEuropeanIdentityCard',
    'UK'                    => 'ID3\DataLib\GlobalUKData',
    'Australia'             => 'ID3\DataLib\GlobalAustralia',
    'US'                    => 'ID3\DataLib\GlobalUS',
    'IdentityCard'          => 'ID3\DataLib\GlobalIdentityCard',
  );
}