<?php

namespace ID3\DataLib;

class GlobalPersonalDetails extends GlobalBase
{
  public $properties = array('Birth'              => 'ID3\DataLib\GlobalUKBirth',
                             'CountryOfBirth'     => null,
                             'DOBDay'             => null,
                             'DOBDaySpecified'    => null,
                             'DOBMonth'           => null,
                             'DOBMonthSpecified'  => null,
                             'DOBYear'            => null,
                             'DOBYearSpecified'   => null,
                             'Forename'           => null,
                             'Gender'             => 'ID3\DataLib\GlobalGenderEnum',
                             'GenderSpecified'    => null,
                             'MiddleName'         => null,
                             'Surname'            => null,
                             'Title'              => null);
  
}