<?php
  
namespace ID3\DataLib;

class GlobalInternationalPassport extends GlobalBase
{
  public $properties = array(
    'CountryOfOrigin'   => null,
    'ExpiryDay'         => null,
    'ExpiryMonth'       => null,
    'ExpiryYear'        => null,
    'Number'            => null,
  );
}