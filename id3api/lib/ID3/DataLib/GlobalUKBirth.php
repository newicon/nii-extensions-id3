<?php

namespace ID3\DataLib;

class GlobalUKBirth extends GlobalBase
{
  public $properties = array('County'             => 'GlobalUKBirthsIndexCountryEnum',
                             'CountryOfBirth'     => null,
                             'MothersMaidenName'  => null,
                             'MunicipalityOfBirth'=> null,
                             'ProvinceOfBirth'    => null,
                             'SurnameAtBirth'     => null,
                             'TownOfBirth'        => null);
                            
  
}