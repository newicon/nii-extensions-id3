<?php

namespace ID3\DataLib;

class GlobalElectricitySupplier extends GlobalBase
{
  public $properties = array('MailSort' => null,
                             'Number1'  => null,
                             'Number2'  => null,
                             'Number3'  => null,
                             'Number4'  => null,
                             'Postcode' => null);
  
}