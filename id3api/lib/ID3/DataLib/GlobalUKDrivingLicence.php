<?php

namespace ID3\DataLib;

class GlobalUKDrivingLicence extends GlobalBase
{
  public $properties = array(
    'Number'                  => null,
    'MailSort'                => null,
    'Postcode'                => null,
    'Microfiche'              => null,
    'IssueDay'                => null,
    'IssueMonth'              => null,
    'IssueYear'               => null
    );

}