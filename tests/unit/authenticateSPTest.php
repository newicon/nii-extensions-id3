<?

class authenticateSPTest extends ID3TestCase
{

	public function testAuthenticateSP()
	{
    // Profile Version

    $profile_version = new ID3\DataLib\GlobalProfileIDVersion;
    $profile_version->ID = ID3_PROFILE_ID;
    $profile_version->Version = 0;

    // Personal Details
		$gid = new ID3\DataLib\GlobalInputData;  
		$gid->Personal->PersonalDetails->Title           = 'Mr';
		$gid->Personal->PersonalDetails->Forename        = 'Joe';
    $gid->Personal->PersonalDetails->MiddleName      = 'John';
		$gid->Personal->PersonalDetails->Surname         = 'Woodhouse';
		$gid->Personal->PersonalDetails->Gender          = 'Male';
		$gid->Personal->PersonalDetails->DOBDay          = 12;
		$gid->Personal->PersonalDetails->DOBMonth        = 12;
		$gid->Personal->PersonalDetails->DOBYear         = 1983;
		$gid->Personal->PersonalDetails->Birth           = null;
		$gid->Personal->PersonalDetails->CountryOfBirth  = 'United Kingdom';

    // Address Details
    $gid->Addresses->CurrentAddress->Building = '51';
    $gid->Addresses->CurrentAddress->Street = 'Talbot Road';
    $gid->Addresses->CurrentAddress->City = 'Bristol';
    $gid->Addresses->CurrentAddress->ZipPostcode = 'BS42NN';
    $gid->Addresses->CurrentAddress->FirstYearOfResidence = '2013';

    // Driving License
    $gid->IdentityDocuments->UK->DrivingLicence->Number = 'WOODH812123JJ9LZ';
    $gid->IdentityDocuments->UK->DrivingLicence->IssueDay = '09';
    $gid->IdentityDocuments->UK->DrivingLicence->IssueMonth = '11';
    $gid->IdentityDocuments->UK->DrivingLicence->IssueYear = '2012';
    $gid->IdentityDocuments->UK->DrivingLicence->Postcode = 'BS42NN';

    // Passport Details
    $gid->IdentityDocuments->UK->Passport->Number = '461154001';

    $result = $this->id3->authenticateSP( $profile_version, $gid,'test string' );

    print_r($result);
	}
}
