<?php

include('id3api/Autoloader.php');

class NID3 extends CApplicationComponent
{
  public $wsdl       = null;
  public $id3        = null;
  public $username   = null;
  public $password   = null;
  public $profile_id = null;
  public $profile_version = null;


  public function init()
  {
    $ID3Loader = new SplClassLoader('ID3', realpath(__DIR__.'/id3api/lib').'/');
    $ID3Loader->register();

    $this->id3 = new ID3\API($this->username,$this->password,$this->profile_id);
  }

  public function __call($name,$args)
  {
    if(method_exists($this->id3,$name))
      return call_user_func_array(array($this->id3,$name),$args);//$this->id3->$name($args);
    else
      throw new Exception(sprintf('The required method "%s" does not exist for %s', $name, get_class($this))); 
  }

  public function getPassportCountriesForSelect()
  {
    $data = $this->id3->getPassportCountries();
    $countries = $data['GetPassportCountriesResult']['GlobalCountry'];
    $return = array();
    foreach($countries as $index => $country_data)
    {
      $return[$country_data['Name']] = $country_data['Name'];
    }
    return $return;
  }

  public function getAddressesForSelect($postcode,$country = 'United Kingdom')
  {
    $ga = new ID3\DataLib\GlobalAddress;  
    $ga->ZipPostcode = $postcode;
    $ga->Country     = $country;
    $data = $this->id3->addressLookup( $ga );

    if(is_soap_fault($data))
    {
      return array('error' => $data->getMessage());
    }
    $addresses = $data['AddressLookupResult']['GlobalAddress'];

    // When postcode is too short, or finds nothing, a single empty result
    // seems to be returned, rather than an array of results
    if (isset($data['AddressLookupResult']['GlobalAddress']['Building']))
      return array('error' => 'No results found.');

    $return = array('results' => array());
    foreach($addresses as $index => $address)
    {
      $summary = sprintf('%s, %s, %s', 
                          isset($address['Building']) ? $address['Building'] : '',
                          isset($address['Street']) ? $address['Street'] : '',
                          isset($address['City']) ? $address['City'] : '');
      $return['results'][$summary] = $address;
    }
    return $return;
  }

}